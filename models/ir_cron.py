# -*- coding: utf-8 -*-
import logging
import threading
import time
import psycopg2
from datetime import datetime
from dateutil.relativedelta import relativedelta
import pytz

import openerp
from openerp import SUPERUSER_ID, netsvc, api
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT
from openerp.tools.safe_eval import safe_eval as eval
from openerp.tools.translate import _
from openerp.modules import load_information_from_description_file

_logger = logging.getLogger(__name__)

BASE_VERSION = load_information_from_description_file('base')['version']

from openerp import api, exceptions, fields, models, tools, _
from openerp.osv import osv
import psycopg2

_intervalTypes = {
    'work_days': lambda interval: relativedelta(days=interval),
    'days': lambda interval: relativedelta(days=interval),
    'hours': lambda interval: relativedelta(hours=interval),
    'weeks': lambda interval: relativedelta(days=7*interval),
    'months': lambda interval: relativedelta(months=interval),
    'minutes': lambda interval: relativedelta(minutes=interval),
}

class IrCron(models.Model):
    _inherit = 'ir.cron'
    
    lastcall = fields.Datetime('Last Execution Date')
    
    def _process_job(self, job_cr, job, cron_cr):
        """ Run a given job taking care of the repetition.

        :param job_cr: cursor to use to execute the job, safe to commit/rollback
        :param job: job to be run (as a dictionary).
        :param cron_cr: cursor holding lock on the cron job row, to use to update the next exec date,
            must not be committed/rolled back!
        """
        try:
            with api.Environment.manage():
                now = openerp.osv.fields.datetime.context_timestamp(job_cr, job['user_id'], datetime.now())
                nextcall = openerp.osv.fields.datetime.context_timestamp(job_cr, job['user_id'], datetime.strptime(job['nextcall'], DEFAULT_SERVER_DATETIME_FORMAT))
                numbercall = job['numbercall']

                ok = False
                while nextcall < now and numbercall:
                    if numbercall > 0:
                        numbercall -= 1
                    if not ok or job['doall']:
                        self._callback(job_cr, job['user_id'], job['model'], job['function'], job['args'], job['id'])
                    if numbercall:
                        nextcall += _intervalTypes[job['interval_type']](job['interval_number'])
                    ok = True
                addsql = ''
                if not numbercall:
                    addsql = ', active=False'
                cron_cr.execute("UPDATE ir_cron SET nextcall=%s, numbercall=%s"+addsql+" WHERE id=%s",
                           (nextcall.astimezone(pytz.UTC).strftime(DEFAULT_SERVER_DATETIME_FORMAT), numbercall, job['id']))
                # Update Last Call in Cron
                if job.get('function') and job.get('function') in ['_cron_issue_create', '_cron_time_tracking', '_cron_project_create']:
                    cron_cr.execute("UPDATE ir_cron SET lastcall=%s WHERE id=%s",
                               (datetime.utcnow().strftime(DEFAULT_SERVER_DATETIME_FORMAT), job['id']))
                self.invalidate_cache(job_cr, SUPERUSER_ID)

        finally:
            job_cr.commit()
            cron_cr.commit()
