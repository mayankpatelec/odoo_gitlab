# -*- coding: utf-8 -*-

import logging
import traceback
import gitlab
import ipaddress
import openerp
from openerp import api, exceptions, fields, models, tools, SUPERUSER_ID
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT
from datetime import datetime
from datetime import timedelta
from dateutil.relativedelta import relativedelta
import dateutil.parser
import requests
import json
import re
_logger = logging.getLogger(__name__)


class Webhook_gitlab(models.Model):
    _name = 'webhook.gitlab'

    @api.multi
    def check_user_connection(self):
        uid_obj = self.env['res.users']
        uid = uid_obj.browse(self._uid)
        gl = gitlab.Gitlab(self.webhook_url, uid.gitlab_token)
        try:
            projects = gl.projects.list()
        except:
            raise exceptions.Warning(_('Please Check Gitlab/User Configuration.'))

    @api.multi
    def group_create(self, gl):
        uid_obj = self.env['res.users']
        groups = gl.groups.list()
        for g in groups:
            vals1 = {}
            members = gl.group_members.list(group_id=g.id)
            assignees_list = []
            for assignee in members:
                assignee_id = uid_obj.search([('gitlab_id', '=', assignee.id)])
                assignees_list.append(assignee_id.id)
            exist = self.env['project.gitlab.group'].search([('gitlab_id', '=', g.id)])
            if not exist:
                vals = {
                    'name': g.name,
                    'gitlab_id': g.id,
                    'description': g.description or '',
                    'user_ids': [(6, 0, assignees_list)],
                }
                self.env['project.gitlab.group'].create(vals)
            if exist and exist.name != g.name or exist.description != g.description:
                vals1.update({'name': g.name,
                              'description': g.description})
            if exist:
                vals1.update({'user_ids': [(6, 0, assignees_list)]})
            if vals1 and exist:
                exist.with_context({'gitlab': False}).write(vals1)
        group_ids = self.env['project.gitlab.group'].search([('gitlab_id', '=', 0)])
        for g_id in group_ids:
            new_group = gl.groups.create({'name': g_id.name, 'path': g_id.name})
            g_id.write({'gitlab_id': new_group.id})
    
    @api.model
    def _cron_members_create(self):
        Cron_pool = self.env['ir.cron']
        ctx = self.env.context.copy()
        self = self.sudo().search([], limit=1)
        self.user_create()
        return True
        
    @api.multi
    def user_create(self):
        print '\n user----------create....'
        if self.state == 'done':
            self.check_user_connection()
            uid_obj = self.env['res.users']
            uid = uid_obj.browse(self._uid)
            gl = gitlab.Gitlab(self.webhook_url, uid.gitlab_token)
            projects = gl.projects.list()
            for p in projects:
                members = p.members.list()
                for m in members:
                    user = uid_obj.search([('gitlab_id', '=', m.id)])
                    if not user:
                        new_id = uid_obj.create({'name': m.name,
                                                 'login': m.name,
                                                 'gitlab_username': m.username,
                                                 'gitlab_id': m.id})
                    else:
                        user.write({'gitlab_id': m.id})

    @api.model
    def _cron_project_create(self):
        Cron_pool = self.env['ir.cron']
        ctx = self.env.context.copy()
        cron = Cron_pool.sudo(SUPERUSER_ID).search([('function', '=', '_cron_project_create'), ('model', '=', 'webhook.gitlab')], limit=1)
        if cron:
            ctx.update({'project_cron': cron})
        self = self.sudo().search([], limit=1)
        self.with_context(ctx).project_create()
        return True

    @api.multi
    def project_create(self):
        if self.state == 'done':
            # self.check_user_connection()
            uid_obj = self.env['res.users']
            tag_obj = self.env['project.gitlab.tag']
            uid = uid_obj.browse(self._uid)
            gl = gitlab.Gitlab(self.webhook_url, uid.gitlab_token)
            uid_obj = self.env['res.users']
            project_obj = self.pool.get('project.project')
            #self.group_create(gl)
            # Sync Projects Gitlab to Odoo
            projects = gl.projects.list()
            for p in projects:
                m = p.members.list()
                tag_ids = []
                # Sync Project Tags Gitlab to Odoo
                for tag in p.tags.list():
                    tag_id = tag_obj.sudo().search([('name', '=', tag)])
                    if not tag_id:
                        tag_id = tag_obj.create({'name': tag.name,
                                                 'note': tag.release.description,
                                                 'message': tag.message})
                    else:
                        tag_id = tag_id[0]
                    if tag_id not in tag_ids:
                        tag_ids.append(tag_id[0])
                group_id = False
                members = []
                # Sync Group >> Not Required
                if p.namespace.kind == 'group':
                    members = gl.group_members.list(group_id=p.namespace.id)
                    group_id = self.env['project.gitlab.group'].search([('gitlab_id','=',p.namespace.id)])
                members = members + m
                assignees_list = []
                # Sync Members Gitlab to Odoo
                for assignee in members:
                    assignee_id = uid_obj.search([('gitlab_id', '=', assignee.id)])
                    assignees_list.append(assignee_id.id)
                user = uid_obj.search([('gitlab_id', '=', p.creator_id)])
                project_id = self.env['project.project'].sudo().search(
                    [('gitlab_id', '=', p.id)])
                if project_id:
                    # Update Project in Odoo
                    project_id.with_context({'gitlab': False}).write({'name': p.name,
                                                                      'tag_ids': [(6, 0, [tag_id.id for tag_id in
                                                                                          tag_ids])],
                                                                      'members': [(6, 0, assignees_list)]})
                if not project_id:
                    # Create Project In Odoo
                    project_obj.create(self._cr, user.id, {
                        'name': p.name,
                        'gitlab_id': p.id,
                        'gitlab_group_id': group_id and group_id.id or False,
                        'members': [(6, 0, assignees_list)],
                        'tag_ids': [(6, 0, [categ_id.id for categ_id in tag_ids])],
                    })
                elif not project_id.gitlab_group_id or not project_id.gitlab_id:
                    project_id.with_context({'gitlab': False}).write({
                        'gitlab_group_id': group_id and group_id.id,
                        'gitlab_id': p.id
                    })
            # Sync New Projects Odoo TO Gitlab
            projects = self.env['project.project'].search([('gitlab_id', '=', False)])
            for project in projects:
                vals = {'name': project.name}
                if project.gitlab_group_id:
                    vals.update({'namespace_id': project.gitlab_group_id.gitlab_id})
                new_project = gl.projects.create(vals)
                project.write({'gitlab_id': new_project.id})
                for m in project.members:
                    if m.gitlab_id:
                        member = new_project.members.create({'user_id': m.gitlab_id, 'access_level':
                            gitlab.DEVELOPER_ACCESS})


    @api.model
    def _cron_issue_create(self):
        Cron_pool = self.env['ir.cron']
        ctx = self.env.context.copy()
        cron = Cron_pool.sudo(SUPERUSER_ID).search([('function', '=', '_cron_issue_create'), ('model', '=', 'webhook.gitlab')], limit=1)
        if cron:
            ctx.update({'issues_cron': cron})
        self = self.sudo().search([], limit=1)
        self.with_context(ctx).issue_create()
        return True

    @api.multi
    def issue_create(self):
        ctx = self.env.context.copy()
        issue_last_sync = False
        # Check Last Sync Time
        if ctx.get('issues_cron'):
            cron = ctx.get('issues_cron')
            issue_last_sync = cron.lastcall
        if self.state == 'done' and self.issue:
            self.check_user_connection()
            task_obj = self.pool.get('project.task')
            uid_obj = self.env['res.users']
            tag_obj = self.env['project.category']
            task_state_obj = self.env['project.task.type']
            uid = uid_obj.browse(self._uid)
            headers = {
                'PRIVATE-TOKEN': uid.gitlab_token,
            }
            gl = gitlab.Gitlab(self.webhook_url, uid.gitlab_token)
            projects = gl.projects.list()
            for p in projects:
                issuet_json = requests.get('https://gitlab.com/api/v4/projects/' + str(p.id) +'/issues', headers=headers)
                if issuet_json.status_code == 200:
                    content = json.loads(issuet_json.content)
                    for issue in content:
                        # Skip already sync Issues
                        if issue.get('updated_at') and issue_last_sync:
                            if dateutil.parser.parse(issue.get('updated_at')).strftime('%Y-%m-%d %H:%M:%S') < issue_last_sync:
                                continue
                        project_id = self.env['project.project'].sudo().search(
                            [('gitlab_id', '=', p.id)])
                        label_ids = []
                        task_stage = {}
                        # Search Task Labels
                        for label in issue['labels']:
                            label_id = tag_obj.sudo().search([('name', '=', label)])
                            if not label_id:
                                label_id = tag_obj.create({'name': label})
                            else:
                                label_id = label_id[0]
                            if label_id not in label_ids:
                                label_ids.append(label_id[0])
                            # Update Task Stage with Issue Labels
                            if project_id and project_id.type_ids and label in project_id.type_ids.mapped('gitlab_issue_label'):
                                state_id = project_id.type_ids.filtered(lambda x: x.gitlab_issue_label == label)[0]
                                task_stage[state_id.sequence] = state_id.id
                            elif project_id and project_id.type_ids and label not in project_id.type_ids.mapped('gitlab_issue_label'):
                                # Create Stage
                                task_state_id = task_state_obj.search([('gitlab_issue_label', '=', label)], limit=1)
                                if not task_state_id:
                                    sequence = task_state_obj.search([], order='sequence desc', limit=1).sequence + 5
                                    task_stage[sequence] = task_state_obj.create({'name': label, 'gitlab_issue_label': label, 'sequence': sequence}).id
                                else:
                                    task_stage[task_state_id.sequence] = task_state_id.id
                                    
                        stage_id = False
                        if task_stage:
                            stage_id = task_stage[max(task_stage.keys())]
                        # Search For Task
                        issue_id = self.env['project.task'].sudo().search(
                            [('gitlab_id', '=', issue['id'])])
                        user = uid_obj.search([('gitlab_id', '=', issue['author']['id'])])
                        assignees_list = []
                        for assignee in issue['assignees']:
                            assignee_id = uid_obj.search([('gitlab_id', '=', assignee['id'])])
                            assignees_list.append(assignee_id.id)
                        vals = {
                            'name': issue['title'],
                            'categ_ids': [(6, 0, [categ_id.id for categ_id in label_ids])],
                            'description': issue['description'] or '',
                            'project_id': project_id.id,
                            'date_deadline': issue['due_date'],
                            'gitlab_id': issue['id'],
                            'user_ids': [(6, 0, assignees_list)],
                        }
                        
                        # Overwrite Task State if it's closed
                        if issue['state'] and issue['state'] == 'closed' and task_state_obj.search([('closed_issue', '=', True)], limit=1):
                            stage_id = task_state_obj.search([('closed_issue', '=', True)], limit=1).id

                        if stage_id:
                            vals.update({'stage_id': stage_id})
                        if not issue_id:
                            ctx.update({'gitlab': False})
                            task_id = task_obj.create(self._cr, user.id, vals, context=ctx)
                        else:
                            ctx.update({'gitlab': False})
                            task_obj.write(self._cr, user.id, issue_id.id, vals, context=ctx)

    @api.model
    def _cron_time_tracking(self):
        Cron_pool = self.env['ir.cron']
        ctx = self.env.context.copy()
        cron = Cron_pool.sudo(SUPERUSER_ID).search([('function', '=', '_cron_time_tracking'), ('model', '=', 'webhook.gitlab')], limit=1)
        if cron:
            ctx.update({'tracking_cron': cron})
        self = self.sudo().search([], limit=1)
        res = self.with_context(ctx).notes_create()
        return True
            

    @api.multi
    def time_tracking(self, result, task, note):
        ctx = self.env.context.copy()
        # Check For Time Spent Sign
        if ctx.get('subtract_time'):
            del ctx['subtract_time']
            sign = -1
            summary = 'Time Subtracted'
        else:
            sign = 1
            summary = 'Time Spend'
        tarcking = ['d', 'w', 'h', 'm']
        vals = {}
        uid_obj = self.env['res.users']
        hours = 5
        date = False
        result1 = result.split()
        task_work_obj = self.env['project.task.work']
        user = uid_obj.search([('gitlab_id', '=', note['author']['id'])])
        for i in tarcking:
            output_names = [name[:-1] for name in result1 if (name[-1] in i)]
            if output_names:
                vals[i] = output_names[0]
        m = 0.00
        if vals.get('h', False) or vals.get('m', False):
            m = vals.get('m', False) or '00'
            h = vals.get('h', False) or '00'
            if (int(m) and int(h)) or int(m):
                hours = h + ':' + m
                signOnP = [int(n) for n in hours.split(":")]
                signOnH = signOnP[0] + signOnP[1] / 60.0
            elif int(h) and not int(m):
                signOnH = int(h)
            date = datetime.today().strftime('%Y-%m-%d')
            task_work_obj.create({'hours': sign * float(signOnH),
                                 'date': date,
                                 'name': summary,
                                 'user_id': user.id,
                                 'task_id': task.id})
        # Add 5days for a week
        if vals.get('w', False) and not vals.get('d', False):
            vals.update({'d': int(vals.get('w', False)) * 5})
        elif vals.get('w', False) and vals.get('d', False):
            vals.update({'d': int(vals.get('d', False)) + (int(vals.get('w', False)) * 5)})
        if vals.get('d', False) and int(vals.get('d', False)) > 1:
            hours = 8
            days = int(vals.get('d', False))
            for x in range(0, days):
                t_date = (datetime.today() + relativedelta(days=x)).strftime('%Y-%m-%d')
                task_work_obj.create({'hours': sign * hours,
                                      'date': t_date,
                                      'name': summary,
                                      'user_id': user.id,
                                      'task_id': task.id})
        elif vals.get('d', False):
            hours = 8
            date = datetime.today().strftime('%Y-%m-%d')
            task_work_obj.create({'hours': sign * hours,
                                  'date': date,
                                  'name': summary,
                                  'user_id': user.id,
                                  'task_id': task.id})

    @api.multi
    def notes_create(self):
        ctx = self.env.context.copy()
        # Check Last Sync Time
        track_last_sync = False
        if ctx.get('tracking_cron'):
            cron = ctx.get('tracking_cron')
            track_last_sync = cron.lastcall
        self.check_user_connection()
        uid_obj = self.env['res.users']
        msg_obj = self.pool.get('mail.message')
        uid = uid_obj.browse(self._uid)
        headers = {'PRIVATE-TOKEN': uid.gitlab_token}
        gl = gitlab.Gitlab(self.webhook_url, uid.gitlab_token)
        projects = gl.projects.list()

        for p in projects:
            issues = p.issues.list()
            for s in issues:
                issue = self.env['project.task'].search([('gitlab_id', '=', s.id)])
                if issue :
                    notes = requests.get('https://gitlab.com/api/v4/projects/%d/issues/%d/notes' %(p.id,s.iid), headers=headers)
                    content = {}
                    if notes.status_code == 200:
                        content = json.loads(notes.content)
                        for note in content:
                            # Skip already sync Notes and Time Tracking
                            if note.get('created_at') and track_last_sync:
                                if dateutil.parser.parse(note.get('created_at')).strftime('%Y-%m-%d %H:%M:%S') < track_last_sync:
                                    continue
                            msg = self.env['mail.message'].search([('gitlab_id', '=', note['id']), ('res_id', '=', issue.id)])
                            if not msg:
                                if 'added' in note['body'] and 'of time spent' in note['body']:
                                    # Add Time Spent
                                    msg = note['body']
                                    start = 'added'
                                    end = 'of time spent'
                                    result = re.search('%s(.*)%s' % (start, end), msg).group(1)
                                    self.time_tracking(result, issue, note)
                                elif 'subtracted' in note['body'] and 'of time spent' in note['body']:
                                    # Subtract Time Spent
                                    msg = note['body']
                                    start = 'subtracted'
                                    end = 'of time spent'
                                    result = re.search('%s(.*)%s' % (start, end), msg).group(1)
                                    self.with_context({'subtract_time': True}).time_tracking(result, issue, note)
                                user = uid_obj.search([('gitlab_id', '=', note['author']['id'])])
                                message = {
                                    'body': note['body'],
                                    'res_id': issue.id,
                                    'model': 'project.task',
                                    'record_name': issue.name,
                                    'type': 'notification',
                                    'gitlab_id': note['id'],
                                    'author_id': user.partner_id.id
                                }
                                msg_obj.create(self._cr, user.id, message)

    @api.multi
    def action_cancel(self):
        self.state = 'cancel'

    @api.multi
    def action_button_confirm(self):
        self.state = 'done'

    name = fields.Char(
        'name',
        required=True,
        help='Name of your consumer webhook. '
             'This name will be used in named of event methods')
    
    webhook_url = fields.Char('Webhook URL')
    active = fields.Boolean(default=True)
    sequence = fields.Integer('Sequence', help="Determine the display order")
    project = fields.Boolean('Project')
    issue = fields.Boolean('issue')
    comment = fields.Boolean('Comment')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('done', 'Done'),
        ('cancel', 'Cancelled'),
        ], string='Status', readonly=True, copy=False, index=True, track_visibility='onchange', default='draft')

