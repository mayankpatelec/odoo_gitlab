# -*- coding: utf-8 -*-

import logging
import traceback
import gitlab
import ipaddress
from openerp import api, exceptions, fields, models, tools
from openerp.tools.translate import _

from openerp.exceptions import Warning, ValidationError

_logger = logging.getLogger(__name__)


class project(models.Model):
    _inherit = 'project.project'

    gitlab_id = fields.Integer('GitLab ID')
    gitlab_group_id = fields.Many2one(
        'project.gitlab.group', 'Group')
    tag_ids = fields.Many2many(
        'project.gitlab.tag', 'project_gitlab_tag_rel', 'project_id', 'tag_id',
        string='Tags')

    def write(self, cr, uid, ids, vals, context=None):
        project_tags_pool = self.pool.get('project.gitlab.tag')
        if vals.get('name', False) or vals.get('tag_ids', False)  and 'gitlab' not in context:
            user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
            gl = gitlab.Gitlab('https://gitlab.com', user.gitlab_token)
            for obj in self.browse(cr, uid, ids, context=context):
                if obj.gitlab_id:
                    project_gitlab = gl.projects.get(obj.gitlab_id)
                    # Sync Project Name and Tags
                    if vals.get('name', False):
                        project_gitlab.name = vals.get('name', False)
                    if vals.get('tag_ids', False) and len(vals.get('tag_ids')[0]) == 3:
                        project_tags = project_gitlab.tag_list
                        for tag in project_tags_pool.browse(cr, uid, vals.get('tag_ids')[0][2], context=context):
                            if tag.name not in project_gitlab.tag_list:
                                project_tags.append(tag.name)
                        project_gitlab.tag_list = project_tags
                    project_gitlab.save()
        super(project, self).write(cr, uid, ids, vals, context=context)
        if vals.get('members', False) and 'gitlab' not in context:
            for obj in self.browse(cr, uid, ids, context=context):
                user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
                gl = gitlab.Gitlab('https://gitlab.com', user.gitlab_token)
                obj = self.browse(cr, uid, ids, context=context)
                project_gitlab = gl.projects.get(obj.gitlab_id)
                # Sync Project Members
                for m in obj.members:
                    if m.gitlab_id:
                        project_gitlab.members.create({'user_id': m.gitlab_id, 'access_level':
                            gitlab.DEVELOPER_ACCESS})
        return True


class project_task(models.Model):
    _inherit = 'project.task'

    gitlab_id = fields.Integer('GitLab ID')
    user_ids = fields.Many2many(
        'res.users', 'res_user_task_rel', 'task_id', 'user_id',
        string='Assigned to')

    states = {
        'Done': 'closed',
        'Cancelled': 'closed',
    }

    def write(self, cr, uid, ids, vals, context=None):
        Project_pool = self.pool.get('project.project')
        if context is None:
            context = {}
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        headers = {
            'PRIVATE-TOKEN': user.gitlab_token,
        }
        gl = gitlab.Gitlab('https://gitlab.com', user.gitlab_token)
        for obj in self.browse(cr, uid, ids, context=context):
            if 'gitlab' not in context:
                try:
                    issue = False
                    if not vals.get('stage_id') and obj.stage_id and obj.stage_id.closed_issue:
                        # Skip sync for Closed task
                        continue
                    if obj.project_id and obj.project_id.gitlab_id and obj.gitlab_id:
                        issue = gl.project_issues.get(obj.gitlab_id, project_id=obj.project_id.gitlab_id)
                        # Update Name to Gitlab
                        name = vals.get('name', False)
                        if name:
                            issue.title = name
                        # Update Desc to Gitlab
                        description = vals.get('description', False)
                        if description:
                            issue.description = description
                        # Update Stage to Gitlab
                        stage_id = vals.get('stage_id', False)
                        if stage_id:
                            temp = self.pool.get('project.task.type').browse(cr, uid, vals['stage_id'], context=context)
                            issue.state = self.states.get(temp.name, 'opened')
                        # Update Deadline Date to Gitlab
                        date_deadline = vals.get('date_deadline', False)
                        if date_deadline:
                            issue.due_date = date_deadline
                        issue.save()
                    elif not obj.gitlab_id and obj.project_id and obj.project_id.gitlab_id:
                        # Create Issue Odoo to Gitlab 
                        title = vals.get('name') or obj.name
                        description = vals.get('description') or obj.description
                        iss_vals = {
                            'title': title,
                            'description': description,
                            }
                        issue = gl.projects.get(obj.project_id.gitlab_id).issues.create(iss_vals)
                        if issue:
                            vals.update({'gitlab_id': issue.id})
                    elif not obj.gitlab_id and not obj.project_id and vals.get('project_id'):
                        # Create Issue Odoo to Gitlab 
                        title = vals.get('name') or obj.name
                        description = vals.get('description') or obj.description
                        iss_vals = {
                            'title': title,
                            'description': description,
                            }
                        project_obj = Project_pool.browse(cr, uid, vals.get('project_id'))
                        if project_obj.gitlab_id:
                            issue = gl.projects.get(project_obj.gitlab_id).issues.create(iss_vals)
                            if issue:
                                vals.update({'gitlab_id': issue.id})
                    # Update Project Change In Issue
                    if obj.project_id and vals.get('project_id') and issue:
                        project_obj = Project_pool.browse(cr, uid, vals.get('project_id'))
                        if project_obj.gitlab_id:
                            issue.move(project_obj.gitlab_id)
                            issue.save()
                except:
                    print "GitlabAuthenticationError"

        result = super(project_task, self).write(cr, uid, ids, vals, context=context)
        try:
            for obj in self.browse(cr, uid, ids, context=context):
                if obj.stage_id and obj.stage_id.closed_issue:
                    # Skip sync for Closed Task
                    continue
                issue = gl.project_issues.get(obj.gitlab_id, project_id=obj.project_id.gitlab_id)
                label_list = issue.labels
                for label in obj.categ_ids:
                    if label.name not in issue.labels:
                        label_list.append(label.name)
                # Update Stage in Labels
                if obj.stage_id.gitlab_issue_label not in issue.labels:
                    label_list.append(obj.stage_id.gitlab_issue_label)
                if label_list:
                    issue.labels = label_list
                issue.save()
        except:
            print "GitlabAuthenticationError"
        return result
        
    @api.model
    def create(self, vals):
        Project_pool = self.env['project.project']
        ctx = self.env.context.copy()
        if vals.get('project_id') and 'gitlab' not in ctx:
            user = self.env.user
            gl = gitlab.Gitlab('https://gitlab.com', user.gitlab_token)
            project_obj = Project_pool.browse(vals.get('project_id'))
            # Issue create Odoo to Gitlab
            if project_obj.gitlab_id:
                iss_vals = {
                    'title': vals.get('name'),
                    'description': vals.get('description'),
                    }
                issue = gl.projects.get(project_obj.gitlab_id).issues.create(iss_vals)
                if issue:
                    vals.update({'gitlab_id': issue.id})
        return super(project_task, self).create(vals)


class project_project(models.Model):
    _inherit = 'res.users'

    gitlab_username = fields.Char('GitLab Username')
    gitlab_token = fields.Char('GitLab Token')
    gitlab_id = fields.Integer('GitLab ID')


class mail_message(models.Model):
    _inherit = 'mail.message'

    gitlab_id = fields.Integer('GitLab ID')


class ProjectTaskType(models.Model):
    _inherit = 'project.task.type'
    
    closed_issue = fields.Boolean('Closed Issue')
    gitlab_issue_label = fields.Char('Gitlab Issue Label')
    
    @api.one
    @api.constrains('closed_issue')
    def _check_closed_issue(self):
        if len(self.search([('closed_issue', '=', True)])) > 1:
            raise ValidationError(_('Only one stage can be Closed Issue stage.'))
            

class project_gitlab_group(models.Model):
    _name = 'project.gitlab.group'

    name = fields.Char('Group', required=True)
    gitlab_id = fields.Integer('GitLab ID')
    description = fields.Text('Description')
    user_ids = fields.Many2many(
        'res.users', 'res_user_group_rel', 'task_id', 'user_id',
        string='Members')

    @api.multi
    def write(self, vals):
        res = super(project_gitlab_group, self).write(vals)
        if vals.get('user_ids'):
            uid_obj = self.env['res.users']
            user = uid_obj.browse(self._uid)
            gl = gitlab.Gitlab('https://gitlab.com', user.gitlab_token)
            group = gl.groups.get(self.gitlab_id)
            members = group.members.list()
            g_members = [m.id for m in members]
            u_list = [uid1.gitlab_id for uid1 in self.user_ids]
            del_list = list(set(g_members)-set(u_list))
            for uid in self.user_ids:
                if uid.gitlab_id not in g_members:
                    group.members.create({'user_id': uid.gitlab_id,
                                          'access_level': gitlab.DEVELOPER_ACCESS})
            for del1 in del_list:
                group.members.delete(del1)
        return res


class project_gitlab_tag(models.Model):
    _name = 'project.gitlab.tag'

    name = fields.Char('Tag name', required=True)
    note = fields.Text('Release notes')
    message = fields.Text('Message')
