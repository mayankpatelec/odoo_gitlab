# -*- coding: utf-8 -*-

{
    'name': 'Odoo Gitlab Integration',
    'version': '8.1.1',
    'author': 'Mayank Patel',
    'category': 'Server Tools',
    'depends': [
        'web', 'project', 'base'
    ],
    'external_dependencies': {
        'python': [
            'ipaddress',
            'requests',
        ],
    },
    'data': [
        'security/ir.model.access.csv',
        'views/webhook_views.xml',
        'views/gitlab_view.xml',
        'views/gitlab_group_views.xml'
    ],
    'auto_install': False
}

