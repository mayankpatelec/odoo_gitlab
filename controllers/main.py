# -*- coding: utf-8 -*-

import pprint

from openerp import http
from openerp.http import request
from openerp import SUPERUSER_ID, exceptions
from openerp.tools.translate import _


class WebhookController(http.Controller):

    @http.route('/odoo_gitlab/run_gitlab', type='json',
        auth='public', csrf=False)
    def run_gitlab(self):
        '''
        :params string webhook_name: Name of webhook to use
        Webhook openerp controller to receive json request and send to
        driver method.
        You will need create your webhook with http://0.0.0.0:0000/webhook
        NOTE: Important use --db-filter params in openerp start.
        '''
        webhook = request.env['webhook.gitlab'].with_env(
            request.env(user=SUPERUSER_ID)).search(
                [('name', '=', 'gitlab')], limit=1)
        webhook.run_gitlab(request,request.jsonrequest)
